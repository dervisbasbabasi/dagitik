import numpy
mu1 = -3
mu2 = 2
sig1 = 0.9
sig2 = 1.1

arr1 = numpy.random.normal(mu1, sig1, 10000)
arr2= numpy.random.normal(mu2, sig2, 10000)

arr1 = numpy.around(arr1)
arr2 = numpy.around(arr2)

hist1 = []
hist2 = []
for i in range(40):
    hist1.append(0)
    hist2.append(0)

for x in range(0, 1000):
    tmp = int(arr1[x]) + 20
    hist1[tmp] += 1
    tmp = int(arr2[x]) + 20
    hist2[tmp] += 1

# http://stackoverflow.com/questions/22241240/how-to-normalize-a-histogram-in-python-updated
#Linkten yararlandım
import matplotlib.pyplot as plt

hist1 = plt.hist(hist1, normed=1)
hist2 = plt.hist(hist2, normed=1)
from numpy import *
plt.xticks( arange(40) )

import pyemd
from pyemd import emd
import numpy as np
distance_matrix = np.array([[-20, 20], [20, -20]])
emd(hist1, hist2, distance_matrix)

plt.show()

