import Queue
import time
import socket
import threading


class LoggerThread (threading.Thread):
    def __init__(self, name, logQueue, logFileName):
        threading.Thread.__init__(self)
        self.name = name
        self.lQueue = logQueue
        self.fid = open(logFileName, 'a')
    
    def log(self, message):
        t = time.ctime()
        self.fid.write(t + ":" + message + "\n")
        self.fid.flush()
    
    def run(self):
        self.log("Starting " + self.name)
        while True:
            if not self.lQueue.empty():
                to_be_logged = self.lQueue.get()
                self.log(to_be_logged)
        self.log("Exiting" + self.name)
        self.fid.close() 

class ReadThread (threading.Thread):
    def __init__(self, name, csoc, address, threadQueue, logQueue):
        threading.Thread.__init__(self)
        self.name = name
        self.csoc = csoc
        self.address = address
        self.lQueue = logQueue
        self.fihrist = fihrist
        self.tQueue = threadQueue
        self.accountname = ""
    
    def parser(self, data):
        data_prt = data[0:3]
        if not self.accountname and not data[0:3] == "USR":
            response = "ERL"
            self.tQueue.put((None,None,response))
            return 0
        if data_prt == "USR" and not self.accountname:
            accountname = data[4:]
            if accountname not in self.fihrist.keys():
                self.accountname = accountname
                response = "HEL " + accountname
                self.tQueue.put((None,None,response))
                self.fihrist[accountname] = self.tQueue

                for key in self.fihrist.keys():
                    tuple_elem_1 = (None,None,"SYS " +
                                self.accountname + " has joined the chat")
                    self.fihrist[key].put(tuple_elem_1)

                tuple_elem_2 = self.accountname + " has joined."
                self.lQueue.put(tuple_elem_2)
                return 0
            
            else:
                response = "REJ " + accountname
                self.tQueue.put((None,None,response))
                return 0

        elif data_prt == "MSG":
            restMessage = data[4:].split(':', 1)
            to_accountname = restMessage[0]
            message = restMessage[1]

            if to_accountname not in self.fihrist.keys():
                response = "MNO " + to_accountname

            else:
                queue_message = (to_accountname, self.accountname, message)

                self.fihrist[to_accountname].put(queue_message)

                response = "MOK"
            self.tQueue.put((None, None, response))
            return 0

        elif data_prt == "TIC":
            response = "TOC"
            self.csend(response)
            return 0

        elif data_prt == "SAY":
            response = "SOK"
            self.tQueue.put((None, None, response))
            messageAll = data[4:]
            for key in self.fihrist.keys():
                tuple_elem_6 = (None, self.accountname, messageAll)
                self.fihrist[key].put(tuple_elem_6)

            return 0

        elif data_prt == "LSQ":
            response = "LSA "
            
            for key in sorted(self.fihrist.keys()):
                response += key + ":"
                
            response = response[:-1]
            tuple_elem_5 = (None,None,response)
            self.tQueue.put(tuple_elem_5)
            return 0
        
        elif data_prt == "QUI":
            response = "BYE " + self.accountname
            self.tQueue.put((None,None,response))

            for key in self.fihrist.keys():
                tuple_elem_3 = (None,None,"SYS " +
                                self.accountname + " has left the chat")
                self.fihrist[key].put(tuple_elem_3)

            del self.fihrist[self.accountname]
            tuple_elem_4 = self.accountname + " has left."
            self.lQueue.put(tuple_elem_4)
            return 1
        
        else:
            response = "ERR"
            self.tQueue.put((None,None,response))
            return 0
        
    def run(self):
        self.lQueue.put("Starting " + self.name)
        while True:
            try:
                new_data = self.csoc.recv(1024)
                queue_message = self.parser(new_data)
                if queue_message:
                    break
            except:
                pass
            
        self.lQueue.put("Exiting " + self.name)
        return

class WriteThread(threading.Thread):
    def __init__(self, name, csoc, address, threadQueue, logQueue):
        threading.Thread.__init__(self)
        self.name = name
        self.csoc = csoc
        self.address = address
        self.lQueue = logQueue
        self.tQueue = threadQueue

    def run(self):
        self.lQueue.put("Starting " + self.name)
        while True:
            if self.tQueue.qsize() > 0:
                queue_message = self.tQueue.get()
                if queue_message[0]:
                    message_to_send = ("MSG " + queue_message[1] + ":" +
                                       queue_message[2]).encode("ascii")

                elif queue_message[1]:
                    message_to_send = ("SAY " + queue_message[1] + ":" +
                                       queue_message[2]).encode("ascii")

                else:
                    message_to_send = ("SYS " + queue_message[1] + ":" +
                                       queue_message[2]).encode("ascii")
                    if queue_message[2][0:3] == "BYE":
                        break
                try:
                    self.csoc.send(message_to_send)
                except socket.error:
                    self.lQueue.put("Problem -> " + self.name)
                    self.csoc.close()
                    break
        try:
            self.csoc.close()
        except:
            pass
        self.lQueue.put("Exiting " + self.name)


logQueue = Queue.Queue()
queueLock = threading.Lock()
fihrist = {}
threadCounter = 1
logThread = LoggerThread("LoggerThread",logQueue,"output.txt")
logThread.setDaemon(True)
logThread.start()
s = socket.socket()
host = "localhost"
port = 12345
s.bind((host, port))
s.listen(5)
while True:
    logQueue.put("Waiting for connection")
    print "Waiting for connection"
    c, addr = s.accept()
    logQueue.put("Got a connection from " +
                 str(addr))
    threadQueue = Queue.Queue()
    readThread = ReadThread("ReadingThread-" +
                            repr(threadCounter), c, addr,threadQueue,logQueue)
    writeThread = WriteThread("WritingThread-" +
                              repr(threadCounter), c, addr,threadQueue,logQueue)
    readThread.start()
    writeThread.start()
    threadCounter = threadCounter + 1

s.close()
