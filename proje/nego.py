#p2p network
import threading
import Queue
import numpy as np
import time
import math
import socket

#CONSTANT
TYPE_NEGO = 'N'
TYPE_PEER = 'P'
STA_WAIT = 'W'
STA_SUCCESS = 'S'
CONN_LOST = 'CONN_LOST'
CONN_CLS = 'CONN_CLOSED'
UPDATE_INT = 600
THREADNUM = 4
QUEUENUM = 20
UIID = '10000'

control_point = False
control_point_img = False
full_name = 'Dervis'
city = 'Istanbul'
date_of_birth = '08.11.1993'
affiliation = 'vip'
status_message = ''
img = {}

s = socket.socket()
ip = socket.gethostname()
port = 12345
s.bind((ip, port))
s.listen(5)
ADDRESS_LIST = {}
control_while_loop = True

def main():
    threads = []
    while True:
        print("Waiting for connetion")
        clientSocket, clientAddress = s.accept()
        s.connect(clientAddress)

        checkingQueue = Queue.Queue()
        threadQueue = Queue.Queue()

        client = Client(queue, socket, ADDRESS_LIST)
        serverListener = Listener(clientSocket, clientAddress, threadQueue, checkingQueue,
                                                   ADDRESS_LIST, uuid)
        sender = Sender(clientSocket, clientAddress, threadQueue, checkingQueue,
                                                 ADDRESS_LIST, uuid)

        threads.append(client)
        threads.append(serverListener)
        threads.append(sender)

        client.start()
        sender.start()
        serverListener.start()

for thread in threads:
            thread.join()


class Listener(threading.Thread):
    def __init__(self, clientSocket, clientAddress, threadQueue, checkingQueue, ADDRESS_LIST, uuid):
        threading.Thread.__init__(self)
        self.checkingQueue = checkingQueue
        self.csocket = clientSocket
        self.caddress = clientAddress
        self.threadQueue = threadQueue
        self.ADDRESS_LIST = ADDRESS_LIST
        self.__connectNode = False
        self.uuid = uuid

    def run(self):
        while True:
            message = str(self.csocket.recv(1024)).strip(' ')
            if message == ''
                self.put_queue(CONN_LOST)
                break
            if len(str(message).split(' ', 1)) == 2: 
                command = str(message).split(' ', 1)[0]
                argument = str(message).split(' ', 1)[1]
            else:
                command = message
                argument = False
            lenghtcmd= len(command)
            if not lenght == 5:
                queueInput = 'E_CMD'

            
            elif not self.__connectNode or return connectPointList[connectNode][0] != STA_SUCCESS:
                queueInput = 'E_CMD'
            elif command == 'Q_HEL':
                queueInput = 'R_HEL %s %s' % UIID % TYPE_NEGO
            elif command == 'Q_QUI':#close
                self.ADDRESS_LIST.pop(self.__connectNode)
                queueInput = 'R_QUI'#return value for close

            # reg
            elif command == 'Q_REG':
                self.__connectNode = str(argument).split(':', 2)
                 cnt == True
                 
                 try:
                     socket.inet_aton(__connectNode[1])
                 except socket.error:
                     cnt = False
                 if not (re.match(r"\d{1,5}", port) and 0 <= int(port) <= 65535 and cnt):
                     cnt = False
                 
                 if cnt == True:
                    if not node_exists(self.__connectNode) or not get_node_status(
                            self.__connectNode) == STA_SUCCESS:
                  
                        self.ADDRESS_LIST.update({self.__connectNode: (STA_WAIT,)})
                        queueInput = 'R_RWA %s' % UUID
                        self.reverse_checking()
                    else:
                        t = time.time()
                        self.ADDRESS_LIST.update({self.__connectNode: (
                            get_node_status(self.__connectNode), t, get_node_type(self.__connectNode))})
                        queueInput = 'R_ROK %s' % t
                else:
                    queueInput = CONN_CLOSE
            elif command == 'Q_LST':
                if argument == '':
                    queueInput = 'E_CMD'
                else:
                    control_point == True          
                    queueInput = ADDRESS_LIST
            elif command == 'Q_NAM':  
                queueInput = 'R_NAM %s' % full_name
            elif command == 'Q_CTY':
                queueInput = 'R_CTY %s' % cty
            elif command == 'Q_DOB':
                queueInput = 'R_DOB %s' % date_of_birthday
            elif command == 'Q_AFF':
                queueInput = 'R_AFF %s' % affiliation
            elif command == 'Q_STA':
                queueInput = 'R_STA %s' % status_message
            elif command == 'Q_IMG':
                control_point_img = True
                queueInput = img
            
            else:
                queueInput = 'E_CMD'

            self.put_queue(queueInput)
            if queueInput == CONN_CLOSED:
                break
        self.csocket.close()

    def put_queue(self, data):
        self.thread_queue.put(data)

    def reverse_checking(self):
        self.checking_queue.put("CLIENT %s:%s:%s" % self.__connectNode)


class Sender(threading.Thread):
    def __init__(self, clientSocket, clientAddress, threadQueue, checkingQueue, ADDRESS_LIST, uuid):
        threading.Thread.__init__(self)
        self.checkingQueue = checkingQueue
        self.csocket = clientSocket
        self.caddress = clientAddress
        self.threadQueue = threadQueue
        self.ADDRESS_LIST = ADDRESS_LIST
        self.__connectNode = False
        self.uuid = uuid

    def run(self):
        if control_point == True:
            sendmes = 'R_LST BEGIN'
            self.csocket.send(sendmes)
        if control_point_img == True:
            sendmes = 'R_IMG BEGIN'
            self.csocket.send(sendmes)

        while True:
            queueData = self.threadQueue.get()
            if queueData == CONN_LOST or queueData == CONN_CLOSED:
                break
            if not self.csend(queueData):
                if control_point == True:
                    sendmes = 'R_LST END'
                    self.csocket.send(sendmes)
                    control_point = False
                if control_point_img == True:
                    sendmes = 'R_IMG END'
                    self.csocket.send(sendmes)
                    control_point_img = False
                self.csocket.close()
                break
            

    def csend(self, data):
        try:
            self.csocket.send(data)
            return True
        except socket.error:
            return False


class Client(threading.Thread):
    def __init__(self, queue, socket, ADDRESS_LIST):
        threading.Thread.__init__(self)
        self.queue = queue
        self.socket = socket
        self.ADDRESS_LIST = ADDRESS_LIST

    def run(self):
        while True:
            if not self.queue.empty():
                queueInput = self.queue.get()
                if queueInput[0:6] == 'CLIENT':
                    connectNode = str(queueInput[7:]).split(':')
                    connectNode2 = str(queueInput[13:]).split(':')
                    try:
                        self._socket.connect(connectNode2)
                        self.socket.send('Q_HEL')
                        response = self.socket.recv(1024)
                        if response[0:5] == 'R_HEL' and is_valid_type(response[6:7]):
                            self.ADDRESS_LIST.update({connectNode: (STATE_SUCCESS, time.time(), response[6:7])})
                    except socket.error:
                        self.ADDRESS_LIST.pop(connectNode)

def get_node_status(connectNode):
    return ADDRESS_LIST[connectNode][0]

def get_node_time(connect_point):
    return ADDRESS_LIST[connectNode][1]

def get_node_type(connect_point):
    return ADDRESS_LIST[connectNode][2]

def node_exists(connect_point):
    return ADDRESS_LIST.has_key(connectNode)

if __name__ == '__main__':
    main()
            






