import string
import sys
import threading
import Queue

exitFlag = 0
class myThread (threading.Thread):
    def __init__(self, threadID, q):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.q = q
    def run(self):
        process_data(self.name, self.q)

def process_data(threadName, q):
    while not exitFlag:
        queueLock.acquire()
        if not q.empty():
            seq = q.get()
            queueLock.release()
            seq_new = seq.translate(cipher)
            cipher_dosya.write(seq_new)
        else:
            queueLock.release()

alphabet = 'abcdefghijklmnopqrstuvwxyz'
s = int(sys.argv[1])
n = int(sys.argv[2])
l = int(sys.argv[3])

cipher_key = alphabet[-s:] + alphabet[:-s]
cipher = string.maketrans(cipher_key, alphabet)

list_1 = []

with open("metin.txt", "rb") as metin_dosya:
    yazi = metin_dosya.read()
    yazi_new = yazi.lower()
    boyut = metin_dosya.tell()
    for i in range(0, boyut, l):
        seq = yazi_new[i: i + l]
        list_1.append(seq)

queueLock = threading.Lock()
workQueue = Queue.Queue(len(list_1))
threads = []
threadID = 1

for i in range(1, n):
    thread = myThread(threadID, workQueue)
    thread.start()
    threads.append(thread)
    threadID += 1

cipher_dosya_isim = "crypted_" + str(s) + "_" + str(n) + "_" + str(l) +".txt"
with open(cipher_dosya_isim, "wb") as cipher_dosya:
    print list_1
    queueLock.acquire()
    for word in list_1:
        workQueue.put(word)
    queueLock.release()

    while not workQueue.empty():
        pass

    exitFlag = 1

    for t in threads:
        t.join()
    print "Exiting Main Thread"





