import string
import sys
from multiprocessing import Lock, Queue, Process

def cipher_method(seq):
    seq_new = seq.translate(cipher)
    cipher_dosya.write(seq_new)
    return True

alphabet = 'abcdefghijklmnopqrstuvwxyz'
#s = int(sys.argv[1])
#n = int(sys.argv[2])
#l = int(sys.argv[3])
s = 1
n = 2
l = 5

cipher_key = alphabet[-s:] + alphabet[:-s]
cipher = string.maketrans(cipher_key, alphabet)

list_1 = []

with open("metin.txt", "rb") as metin_dosya:
    yazi = metin_dosya.read()
    yazi_new = yazi.lower()
    boyut = metin_dosya.tell()
    for i in range(0, boyut, l):
        seq = yazi_new[i: i + l]
        list_1.append(seq)

queueLock = Lock()
work_queue = Queue(len(list_1))
processes = []

for word in list_1:
    work_queue.put(word)

for w in list_1:
    p = Process(target=cipher_method, args=(w))
    p.start()
    processes.append(p)

cipher_dosya_isim = "crypted_" + str(s) + "_" + str(n) + "_" + str(l) +".txt"
with open(cipher_dosya_isim, "wb") as cipher_dosya:
    print list_1
    queueLock.acquire()
    for word in list_1:
        work_queue.put(word)
    queueLock.release()

    while not work_queue.empty():
        pass

    exitFlag = 1

    for t in processes:
        t.join()
    print "Exiting Main Thread"

