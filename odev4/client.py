#!/usr/bin/env python
import socket
import threading
import random
statue = True
class readThread (threading.Thread):

	def __init__(self, threadID, socket):
		super(readThread, self).__init__()
		self.threadID = threadID
		self.socket = socket
	def run(self):
		girdi = "ilk_deger"
		while girdi != "Bitir":
			girdi = raw_input("Your input: ")
			self.socket.send(girdi)
		global statue
		statue = False
		self.socket.close()

class writeThread (threading.Thread):
	def __init__(self, threadID, socket):
		super(writeThread, self).__init__()
		self.threadID = threadID
		self.socket = socket
	def run(self):
		while statue:
			print(self.socket.recv(1024))
s = socket.socket()
host = "127.0.0.1"
port = 12342
s.connect((host, port))
rThread = readThread("R",s)
rThread.start()
wThread = writeThread("W",s)
wThread.start()

rThread.join()
wThread.join()
