#!/usr/bin/env python
import socket,sys,threading,time

class myThread (threading.Thread):
	def __init__(self, threadID, clientSocket, clientAddr):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.clientSocket = clientSocket
		self.clientAddr = clientAddr
	def run(self):
		print ("Starting Thread-" + str(self.threadID))
		self.clientSocket.send("Merhaba su an saat: " + time.ctime(time.time()))#random olarak eklenecek
		print ("Ending Thread-" + str(self.threadID))
		buffer = self.clientSocket.recv(1024)
		while "Bitir" != buffer:
			if buffer is not None:
				self.clientSocket.send("Peki " +
									   self.clientAddr[0] + "," +
									   str(self.clientAddr[1]) + buffer)
			else:
				self.clientSocket.send("Veri gelmedi " +
									   self.clientAddr[0] + "," +
									   str(self.clientAddr[1])
									   )
			buffer = self.clientSocket.recv(1024)

		self.clientSocket.send(" ")
			
threadlist = []
s = socket.socket()
host = "0.0.0.0"
port = 12342
s.bind((host, port))
s.listen(5)
threadCounter = 0
while True:
	print "Waiting for connection"
	c, addr = s.accept()
	print 'Got a connection from ', addr
	threadCounter += 1
	thread = myThread(threadCounter, c, addr)
	thread.start()
	threadlist.append(thread)
for t in threadlist:
	t.join()
s.close()